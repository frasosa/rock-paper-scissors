// function for computer to make a random choice
function getComputerChoice() {
    // get random number 0, 1, or 2
    const choice = Math.floor(Math.random() * 3)
    if (choice === 1) {
        return "rock"
    } else if (choice === 2) {
        return "paper"
    } else {
        return "scissors"
    }
}

// function to handle logic of a round 
function playRound(playerChoice, computerChoice) {
    playerChoice = playerChoice.toLowerCase()
    if (playerChoice === computerChoice) {
        return "Its a tie!"
    } else if (playerChoice === "rock") {
        if (computerChoice === "paper") {
            return "You lose! Paper beats rock."
        } else {
            return "You win! Rock beats scissors."
        }
    } else if (playerChoice === "paper") {
        if (computerChoice === "rock") {
            return "You win! Paper beats rock."
        } else {
            return "You lose! Scissors beats paper."
        }
    } else {
        if (computerChoice === "paper") {
            return "You win! Scissors beats paper."
        } else {
            return "You lose! Rock beats scissors."
        }
    }
}

function playGame() {
    const playerChoice = this.id;
    const computerChoice = getComputerChoice();

    // update images
    const playerImg = document.getElementById('player');
    const computerImg = document.getElementById('computer');
    playerImg.src = "./icons/" + playerChoice + ".png";
    computerImg.src = "./icons/" + computerChoice + ".png";

    const result = document.querySelector('.result')
    console.log(result)
    result.textContent = playRound(playerChoice, computerChoice)
}

// add event listeners
const rockButton = document.getElementById('rock');
rockButton.addEventListener('click', playGame);

const paperButton = document.getElementById('paper');
paperButton.addEventListener('click', playGame);

const scissorsButton = document.getElementById('scissors');
scissorsButton.addEventListener('click', playGame);
