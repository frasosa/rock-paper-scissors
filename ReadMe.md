# Rock paper scissors

A simple rock paper scissors game that allows you to play single round games 
against a computer.

Playable at: https://frasosa.gitlab.io/rock-paper-scissors/